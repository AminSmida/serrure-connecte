#include <Arduino.h>
#include "esp_bt_main.h"
#include "esp_bt_device.h"
#include "BluetoothSerial.h"

// put function declarations here:
// int myFunction(int, int);
// Définir la broche de connexion du capteur PIR
const int pirPin = 4; // Par exemple, la broche 13 (choisissez une broche GPIO disponible sur votre ESP32)
int val = 0;           // Variable pour lire l'état du capteur PIR
int ledv = 5;
int ledr = 2;

BluetoothSerial SerialBT;

void printDeviceAddress() {
  const uint8_t* point = esp_bt_dev_get_address();
  for (int i = 0; i < 6; i++) {
    char str[3];
    sprintf(str, "%02X", (int)point[i]);
    Serial.print(str);
    if (i < 5){
      Serial.print(":");
    }
  }
}

void setup()
{
  pinMode(ledv, OUTPUT);
  pinMode(ledr, OUTPUT);
  pinMode(pirPin, INPUT); // Définir la broche PIR comme une entrée
  Serial.begin(9600);   // Initialiser la communication série à 115200 bps
  SerialBT.begin("ESP32BT");
  Serial.print("MaxAddr : ");
  printDeviceAddress();
}

void loop()
{
  int sensorValue = digitalRead(pirPin); // Lire l'état de la broche PIR

  if (sensorValue == HIGH)
  {
      Serial.println("Mouvement détecté!");
      digitalWrite(ledr, HIGH);
      if (Serial.available()) {
        SerialBT.write(Serial.read());
      }
      if (SerialBT.available()) {
        Serial.write(SerialBT.read());
      }
      delay(10000);
  }
  else
  {
      Serial.println("Aucun mouvement.");
      digitalWrite(ledr, LOW);
  }
}